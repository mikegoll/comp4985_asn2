#pragma once
/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: UI.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   void updateUI(HWND, bool)
--              void setText(HWND, int, char *)
--              void setCheck(HWND, int, int)
--              bool getCheck(HWND, int)
--              void updateProgressBar(HWND, int, int, int, int)
--              void updateStatusText(HWND, char *)
--              void getUserInput(HWND)
--              int getPacketSize(const HWND)
--              int getPacketNum(const HWND)
--              int getStatNum(const HWND, int)
--              void showMessageBox(HWND, char *, char *, int)
--              bool validateDest(char *)
--              void setStatNum(const HWND, const int, const unsigned int)
--              void setStatNum(const HWND, const int, const unsigned int)
--              void appendMessage(HWND, std::string)
--              int getFilePath(HWND hwnd)
--              char * getFileName(HWND hDlg)
--
-- DATE:        February 1, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Responsible for changing any element on the user interface.
--------------------------------------------------------------------------------------------------*/

//Windows Headers
#include <Windows.h>
#include <windowsx.h>
#include <ShObjIdl.h>
#include <commctrl.h>

//C/C++ Headers
#include <string.h>
#include <string>
#include <sstream>

//Custom Headers
#include "resource.h"

/* ----------------------------------------------------------------------------
-- FUNCTION:   updateUI
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void updateUI(HWND, bool)
--
-- PARAMETER:  HWND hDlg   - A handle to the dialog to use with the UI updating
--                           functions.
--
--             bool option - the option to either disable or enable the user
--                           interface elements
--
-- RETURNS:    void
--
-- NOTES:      Updates all user interface elements to disabled or enabled
-----------------------------------------------------------------------------*/
void updateUI(HWND, bool);


/* ----------------------------------------------------------------------------
-- FUNCTION:   setText
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  bool getCheck(HWND, int)
--
-- PARAMETER:  HWND hDlg     - A handle to the dialog to use with the UIu pdating
--                             functions.
--
--             int item       - The user interface element to be grabbed
--             char * message - The message to set the text to.
--
-- RETURNS:    void
--
-- NOTES:      Sets the text of any given window element.
--
-----------------------------------------------------------------------------*/
void setText(HWND, int, char *);

/*-----------------------------------------------------------------------------------------
-- FUNCTION:   setCheck
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void setCheck(HWND, int, int)
--
-- PARAMETER:  HWND hDlg  - A handle to the dialog to use with the UI updating
--                          functions.
--
--             int item   - The user interface element to be grabbed
--             int option - The option to either check or uncheck the element
--
-- RETURNS:    void
--
-- NOTES:      Sets a window element to either checked or not checked.
--             The option is handled as a boolean.
-----------------------------------------------------------------------------------------*/
void setCheck(HWND, int, int);


/* ----------------------------------------------------------------------------
-- FUNCTION:   getCheck
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UIu pdating
--                         functions.
--
--             int item  - The element to be checked to see if it is checked.
--
-- INTERFACE:  bool getCheck(HWND, int)
--
-- RETURNS:    void
--
-- NOTES:      Gets whether or not a window element is checked.
--
-----------------------------------------------------------------------------*/
bool getCheck(HWND, int);


/*---------------------------------------------------------------------------------------------
-- FUNCTION:   updateProgressBar
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void updateProgressBar(HWND, int, int, int, int)
--
-- PARAMETER:  HWND hDlg  - A handle to the dialog to use with the UI
--                          updating functions.
--
--             int option - The option to operate on the progress bar (ex. PBM_SETPOS)
--             int value  - The value to set the progress bar to.
--             int value2 - The second value to send to the progress bar
--
-- RETURNS:    void
--
-- NOTES:      Updates the progress bar to any specified value.
--             Value2 is always 0 in the application.
---------------------------------------------------------------------------------------------*/
void updateProgressBar(HWND, int, int, int);



/* ----------------------------------------------------------------------------
-- FUNCTION:   updateStatusText
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void updateStatusText(HWND, char *)
--
-- PARAMETER:  HWND   hDlg    - A handle to the dialog to use with the UI
updating functions.
--             char * message - The message to update the text with.
--
-- RETURNS:    void
--
-- NOTES:      Updates the text above the progress bar.
-----------------------------------------------------------------------------*/
void updateStatusText(HWND, char *);


/* ----------------------------------------------------------------------------
-- FUNCTION:   getUserInput
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void getUserInput(HWND)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
functions.
--
-- RETURNS:    void
--
-- NOTES:      Gets the user input from the various text fields
-----------------------------------------------------------------------------*/
void getUserInput(HWND);

/* ----------------------------------------------------------------------------
-- FUNCTION:   getPacketSize
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void getPacketSize(HWND)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
--                         functions.
--
-- RETURNS:    void
--
-- NOTES:      Gets the packet size specifed by the user
-----------------------------------------------------------------------------*/
int getPacketSize(const HWND);

/* ----------------------------------------------------------------------------
-- FUNCTION:   getPacketNum
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void getPacketNum(HWND)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
--                         functions.
--
-- RETURNS:    void
--
-- NOTES:      Gets the number of packets specifed by the user.
-----------------------------------------------------------------------------*/
int getPacketNum(const HWND);

/* ----------------------------------------------------------------------------
-- FUNCTION:   getStatNum
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  int getStatNum(const HWND, int)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
--                         functions.
--             int id    - the specified id of the user interface element.
--
-- RETURNS:    void
--
-- NOTES:      Gets the stat number of a specified statistic.
-----------------------------------------------------------------------------*/
int getStatNum(const HWND, int);

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   showMessageBox
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void showMessageBox(HWND, char *, char *, int)
--
-- PARAMETER:  HWND hDlg      - A handle to the dialog to spawn the message box.
--             char * message - The message to be displayed in the message box.
--             char * title   - The title of the message box
--             int iconOption - The icon to be displayed in the message box (ex. MB_OK)
--
-- RETURNS:    void
--
-- NOTES:      Spawns a message box with a specified message, title and icon.
--------------------------------------------------------------------------------------------*/
void showMessageBox(HWND, char *, char *, int);

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   validateDest
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void setStatNum(const HWND, const int, const unsigned int)
--
-- PARAMETER:  char * dest - The destination specified by the user
--
-- RETURNS:    boolean - Whether or not the destination specifed is valid.
--
-- NOTES:      Validates that the destination starts with a character or digit.
--------------------------------------------------------------------------------------------*/
bool validateDest(char *);

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   setStatNum
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void setStatNum(const HWND, const int, const unsigned int)
--
-- PARAMETER:  HWND hDlg           - A handle to the dialog to use with the UI
--                                    updating functions.
--
--             int id              - the specified id of the user interface element.
--             unsigned int newNum - The new value to set the number to.
--
-- RETURNS:    void
--
-- NOTES:      Sets the number of a specifed statistic.
--------------------------------------------------------------------------------------------*/
void setStatNum(const HWND, const int, const unsigned int);

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   appendMessage
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void appendMessage(HWND, std::string)
--
-- PARAMETER:  HWND hDlg           - A handle to the dialog to spawn the message box.
--             std::string message - The message to be appended to the central edit control
(textbox)
--
-- RETURNS:    void
--
-- NOTES:      Appends a message to the central edit control (textbox).
--------------------------------------------------------------------------------------------*/
void appendMessage(HWND, std::string);


/*--------------------------------------------------------------------------------------------
-- FUNCTION:   getFilePath
--
-- DATE:       February 22, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  int getFilePath(HWND)
--
-- PARAMETER:  HWND hwnd - A handle to the dialog to spawn the message box.
(textbox)
--
-- RETURNS:    void
--
-- NOTES:      Opens a dialog and allows the user to select a file.
--------------------------------------------------------------------------------------------*/
int getFilePath(HWND hwnd);

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   getFileName
--
-- DATE:       February 22, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  char * getFileName(HWND hDlg)
--
-- PARAMETER:  HWND hwnd - A handle to the dialog to spawn the message box.
--                         (textbox)
--
-- RETURNS:    void
--
-- NOTES:      Grabs the path from the textbox on the user interface.
--------------------------------------------------------------------------------------------*/
char * getFileName(HWND hDlg);

