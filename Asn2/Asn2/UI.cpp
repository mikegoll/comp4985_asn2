/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: UI.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   void updateUI(HWND, bool)
--              void setText(HWND, int, char *)
--              void setCheck(HWND, int, int) 
--              bool getCheck(HWND, int)
--              void updateProgressBar(HWND, int, int, int, int)
--              void updateStatusText(HWND, char *)
--              void getUserInput(HWND)
--              int getPacketSize(const HWND)
--              int getPacketNum(const HWND)
--              int getStatNum(const HWND, int)
--              void showMessageBox(HWND, char *, char *, int)
--              bool validateDest(char *)
--              void setStatNum(const HWND, const int, const unsigned int)
--              void setStatNum(const HWND, const int, const unsigned int)
--              void appendMessage(HWND, std::string)
--
-- DATE:        February 1, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Responsible for changing any element on the user interface.
--------------------------------------------------------------------------------------------------*/

#include "Client.h"
#include "UI.h"

/* ----------------------------------------------------------------------------
-- FUNCTION:   updateUI
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void updateUI(HWND, bool)
--
-- PARAMETER:  HWND hDlg   - A handle to the dialog to use with the UI updating
--                           functions.
--
--             bool option - the option to either disable or enable the user
--                           interface elements
--
-- RETURNS:    void
--
-- NOTES:      Updates all user interface elements to disabled or enabled
-----------------------------------------------------------------------------*/
void updateUI(HWND hDlg, bool option) {
	Button_Enable(GetDlgItem(hDlg, IDC_ACTION), option);
	Button_Enable(GetDlgItem(hDlg, IDC_CHECK1), option);
	Button_Enable(GetDlgItem(hDlg, IDC_CHECK2), option);
	Button_Enable(GetDlgItem(hDlg, IDC_DEST), option);
	Button_Enable(GetDlgItem(hDlg, IDC_Port), option);
	Button_Enable(GetDlgItem(hDlg, IDC_PACKETSIZE), option);
	Button_Enable(GetDlgItem(hDlg, IDC_PACKETNUM), option);
}




/*-----------------------------------------------------------------------------------------
-- FUNCTION:   setCheck
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void setCheck(HWND, int, int)
--
-- PARAMETER:  HWND hDlg  - A handle to the dialog to use with the UI updating
--                          functions.
--
--             int item   - The user interface element to be grabbed
--             int option - The option to either check or uncheck the element
--
-- RETURNS:    void
--
-- NOTES:      Sets a window element to either checked or not checked.
--             The option is handled as a boolean.
-----------------------------------------------------------------------------------------*/
void setCheck(HWND hDlg, int item, int option) {
	Button_SetCheck(GetDlgItem(hDlg, item), option);
}



/* ----------------------------------------------------------------------------
-- FUNCTION:   setText
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  bool getCheck(HWND, int)
--
-- PARAMETER:  HWND hDlg     - A handle to the dialog to use with the UIu pdating
--                             functions.
--
--             int item       - The user interface element to be grabbed
--             char * message - The message to set the text to.
--
-- RETURNS:    void
--
-- NOTES:      Sets the text of any given window element.
--
-----------------------------------------------------------------------------*/
void setText(HWND hDlg, int item, char * message) {
	SetWindowText(GetDlgItem(hDlg, item), message);
}


/* ----------------------------------------------------------------------------
-- FUNCTION:   getCheck
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UIu pdating 
--                         functions.
--
--             int item  - The element to be checked to see if it is checked.
--
-- INTERFACE:  bool getCheck(HWND, int)
--
-- RETURNS:    void
--
-- NOTES:      Gets whether or not a window element is checked.
--
-----------------------------------------------------------------------------*/
bool getCheck(HWND hDlg, int item) {
	return (Button_GetCheck(GetDlgItem(hDlg, item)));
}


/*---------------------------------------------------------------------------------------------
-- FUNCTION:   updateProgressBar
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void updateProgressBar(HWND, int, int, int, int)
-- 
-- PARAMETER:  HWND hDlg  - A handle to the dialog to use with the UI
                            updating functions.
--
--             int option - The option to operate on the progress bar (ex. PBM_SETPOS)
--             int value  - The value to set the progress bar to.
--             int value2 - The second value to send to the progress bar
--
-- RETURNS:    void
--
-- NOTES:      Updates the progress bar to any specified value.
--             Value2 is always 0 in the application.
---------------------------------------------------------------------------------------------*/
void updateProgressBar(HWND hDlg, int option, int value, int value2) {
	SendMessage(GetDlgItem(hDlg, IDC_PROGRESS), option, value, value2);
}


/* ----------------------------------------------------------------------------
-- FUNCTION:   updateStatusText
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void updateStatusText(HWND, char *)
--
-- PARAMETER:  HWND   hDlg    - A handle to the dialog to use with the UI 
                                updating functions.
--             char * message - The message to update the text with.
--
-- RETURNS:    void
--
-- NOTES:      Updates the text above the progress bar.
-----------------------------------------------------------------------------*/
void updateStatusText(HWND hDlg, char * message) {
	SetDlgItemText(hDlg, IDC_EDIT2, message);
}


/* ----------------------------------------------------------------------------
-- FUNCTION:   getUserInput
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void getUserInput(HWND)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating 
                           functions.
--
-- RETURNS:    void
--
-- NOTES:      Gets the user input from the various text fields
-----------------------------------------------------------------------------*/
void getUserInput(HWND hDlg) {
	char dest[32], port[32], pNum[32], pSize[32];
	int inputSize = 32;

	int portNum, packNum, packSize;

	updateStatusText(hDlg, "Getting User Input...");
	updateProgressBar(hDlg, PBM_SETPOS, 0, 0);

	GetDlgItemText(hDlg, IDC_DEST, dest, inputSize);
	updateProgressBar(hDlg, PBM_SETPOS, 25, 0);

	if (getCheck(hDlg, IDC_CHECK2)) {
		if (!validateDest(dest)) {
			showMessageBox(hDlg, "The specified destination is invalid.\nPlease ensure it is correct.", "Destination Error", MB_ICONERROR);
			return;
		}
	}

	GetDlgItemText(hDlg, IDC_Port, port, inputSize);
	if (strcmp(port, "") != 0)
		updateProgressBar(hDlg, PBM_SETPOS, 50, 0);
	else {
		showMessageBox(hDlg, "Port Number is Empty, Please Specify a Port Number", "Port Number Error", MB_OK | MB_ICONERROR);
		return;
	}

	GetDlgItemText(hDlg, IDC_PACKETSIZE, pNum, inputSize);
	if (strcmp(pNum, "") != 0)
		updateProgressBar(hDlg, PBM_SETPOS, 75, 0);
	else {
		if (!(getCheck(hDlg, IDC_CHECK1))) {
			showMessageBox(hDlg, "Packet Number is Empty, Please Specify a Number of Packets", "Packet Number Error", MB_OK | MB_ICONERROR);
			return;
		}
	}

	GetDlgItemText(hDlg, IDC_PACKETNUM, pSize, inputSize);
	if (strcmp(pSize, "") != 0)
		updateProgressBar(hDlg, PBM_SETPOS, 100, 0);
	else {
		if (!(getCheck(hDlg, IDC_CHECK1))) {
			showMessageBox(hDlg, "Packet Size is Empty, Please Specify a Packet Size", "Packet Size Error", MB_OK | MB_ICONERROR);
			return;
		}
	}

	portNum = atoi(port);
	
	appendMessage(hDlg, "Initializing...");

	//determine what mode the user wants
	if (getCheck(hDlg, IDC_CHECK2)) {
		//Start client
		if (getCheck(hDlg, IDC_TCP)) {
			clientStartTCP(dest, portNum, hDlg);
			updateUI(hDlg, TRUE);
		} else {
			clientStartUDP(dest, portNum, hDlg);
			updateUI(hDlg, TRUE);
		}

	} else if (getCheck(hDlg, IDC_CHECK1)) {
		//Start server
		if (getCheck(hDlg, IDC_TCP)) {
			serverStartTCP(dest, portNum, hDlg);
			updateUI(hDlg, TRUE);
		} else {
			serverStartUDP(dest, portNum, hDlg);
			updateUI(hDlg, TRUE);
		}

	} else {
		MessageBox(hDlg, "Please Select an Option", "Selection Error", MB_OK | MB_ICONEXCLAMATION);
		updateUI(hDlg, TRUE);
	}
}

/* ----------------------------------------------------------------------------
-- FUNCTION:   getPacketSize
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void getPacketSize(HWND)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
--                         functions.
--
-- RETURNS:    void
--
-- NOTES:      Gets the packet size specifed by the user
-----------------------------------------------------------------------------*/
int getPacketSize(const HWND hDlg) {
	char pNum[32];
	GetDlgItemText(hDlg, IDC_PACKETSIZE, pNum, sizeof(pNum));
	return atoi(pNum);
}

/* ----------------------------------------------------------------------------
-- FUNCTION:   getPacketNum
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void getPacketNum(HWND)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
--                         functions.
--
-- RETURNS:    void
--
-- NOTES:      Gets the number of packets specifed by the user.
-----------------------------------------------------------------------------*/
int getPacketNum(const HWND hDlg) {
	char pSize[32];
	GetDlgItemText(hDlg, IDC_PACKETNUM, pSize, sizeof(pSize));
	return atoi(pSize);
}

/* ----------------------------------------------------------------------------
-- FUNCTION:   getStatNum
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  int getStatNum(const HWND, int)
--
-- PARAMETER:  HWND hDlg - A handle to the dialog to use with the UI updating
--                         functions.
--             int id    - the specified id of the user interface element.
--
-- RETURNS:    void
--
-- NOTES:      Gets the stat number of a specified statistic.
-----------------------------------------------------------------------------*/
int getStatNum(const HWND hDlg, int id) {
	char pSize[32];
	GetDlgItemText(hDlg, id, pSize, sizeof(pSize));
	return atoi(pSize);
}

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   setStatNum
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void setStatNum(const HWND, const int, const unsigned int) 
--
-- PARAMETER:  HWND hDlg           - A handle to the dialog to use with the UI 
                                     updating functions.

--             int id              - the specified id of the user interface element.
--             unsigned int newNum - The new value to set the number to.
--
-- RETURNS:    void
--
-- NOTES:      Sets the number of a specifed statistic.
--------------------------------------------------------------------------------------------*/
void setStatNum(const HWND hDlg, const int id, const unsigned int newNum) {
	char buf[32];
	sprintf_s(buf, sizeof(buf), "%d", newNum);
	SetDlgItemText(hDlg, id, buf);
	return;
}

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   validateDest
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void setStatNum(const HWND, const int, const unsigned int)
--
-- PARAMETER:  char * dest - The destination specified by the user
--
-- RETURNS:    void
--
-- NOTES:      Validates that the destination starts with a character or digit.
--------------------------------------------------------------------------------------------*/
bool validateDest(char *dest) {
	return (isalpha(dest[0]) || isdigit(dest[0]));
}

/*--------------------------------------------------------------------------------------------
-- FUNCTION:   showMessageBox
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void showMessageBox(HWND, char *, char *, int)
--
-- PARAMETER:  HWND hDlg      - A handle to the dialog to spawn the message box.
--             char * message - The message to be displayed in the message box.
--             char * title   - The title of the message box
--             int iconOption - The icon to be displayed in the message box (ex. MB_OK)
--
-- RETURNS:    void
--
-- NOTES:      Spawns a message box with a specified message, title and icon.
--------------------------------------------------------------------------------------------*/
void showMessageBox(HWND hDlg, char * message, char * title, int iconOption) {
	MessageBox(hDlg, message, title, MB_OK | iconOption);
	updateUI(hDlg, TRUE);
	updateStatusText(hDlg, "Idle");
	updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
}


/*--------------------------------------------------------------------------------------------
-- FUNCTION:   appendMessage
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void appendMessage(HWND, std::string)
--
-- PARAMETER:  HWND hDlg           - A handle to the dialog to spawn the message box.
--             std::string message - The message to be appended to the central edit control
                                     (textbox)
--
-- RETURNS:    void
--
-- NOTES:      Appends a message to the central edit control (textbox).
--------------------------------------------------------------------------------------------*/
void appendMessage(HWND hDlg, std::string message) {
	int length = GetWindowTextLength(hDlg);
	std::stringstream ss;

	ss << message.c_str() << "\r\n";

	SendMessage(GetDlgItem(hDlg, IDC_EDIT1), EM_SETSEL, (WPARAM)length, (LPARAM)length);
	SendMessage(GetDlgItem(hDlg, IDC_EDIT1), EM_REPLACESEL, 0, (LPARAM)ss.str().c_str());
}


/*--------------------------------------------------------------------------------------------
-- FUNCTION:   getFilePath
--
-- DATE:       February 22, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  int getFilePath(HWND)
--
-- PARAMETER:  HWND hwnd - A handle to the dialog to spawn the message box.
(textbox)
--
-- RETURNS:    void
--
-- NOTES:      Opens a dialog and allows the user to select a file.
--------------------------------------------------------------------------------------------*/
int getFilePath(HWND hwnd) {
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);

	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			// Show the Open dialog box.
			hr = pFileOpen->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);


					if (SUCCEEDED(hr))
					{
						SetWindowTextW(GetDlgItem(hwnd, IDC_FILEPATH), pszFilePath);
					}
					pItem->Release();
				}
			}
			pFileOpen->Release();
		}
		CoUninitialize();
	}
	return 0;
}


/*--------------------------------------------------------------------------------------------
-- FUNCTION:   getFileName
--
-- DATE:       February 22, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  char * getFileName(HWND hDlg)
--
-- PARAMETER:  HWND hwnd - A handle to the dialog to spawn the message box.
--                         (textbox)
--
-- RETURNS:    void
--
-- NOTES:      Grabs the path from the textbox on the user interface.
--------------------------------------------------------------------------------------------*/
char * getFileName(HWND hDlg) {
	char name[300];
	GetDlgItemText(hDlg, IDC_FILEPATH, name, 300);
	return name;
}
