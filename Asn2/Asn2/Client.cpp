/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: Main.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   void clientStartTCP(const char *, const int &, const HWND)
--              void clientStartUDP(const char *, const int &, const HWND)
--              void CALLBACK compRoutineTCP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--              void CALLBACK compRoutineUDP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- DATE:        February 6, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Handles all functionality to the client portion of the application.
--------------------------------------------------------------------------------------------------*/

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include "Client.h"

//-------------- Globals --------------//

//only used by completion routines to update user interface
//I didn't want to use a struct for a single handle

HWND hwnd;   //Handle to the dialog

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   clientStartUDP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void clientStartUDP(const char *, const int &, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int port    - The port number specified by the user
--             HWND hDlg   - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      The handle to the dialog is only used to update the user interface.
--             Starts up the client for UDP.
--             Initializes the socket for UDP.
--             Sends all of the datagrams.
----------------------------------------------------------------------------------------------------------------------*/
void clientStartTCP(const char * dest, const int &port, const HWND hDlg) {
	std::stringstream ss;
	hostent *hp;
	sockaddr_in serverAddr;
	SOCKET s, newS;
	int retVal, packetNum, packetSize, sent;
	WSAOVERLAPPED ol;
	DWORD numSent;
	char * buf, *filename = { 0 };
	time_t timev, timev2;
	WSABUF sendBuf = { 0 };
	bool running = TRUE;
	bool file = FALSE;

	WORD wVersionRequested = MAKEWORD(2, 2);
	WSADATA wsaData;

	hwnd = hDlg;
	memset(&ol, 0, sizeof(WSAOVERLAPPED));

	packetNum  = getPacketNum(hDlg);
	packetSize = getPacketSize(hDlg);

	std::ifstream fin(getFileName(hDlg), std::ifstream::in);

	if (fin)
		file = TRUE;

	if (WSAStartup(wVersionRequested, &wsaData) != 0) {
		appendMessage(hDlg, "Startup Failed");
		updateStatusText(hDlg, "Startup Failed");
		updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
	}

	//updating UI
	ss << "Connecting to " << "\"" << dest << "\"\r\n";
	appendMessage(hDlg, ss.str().c_str());
	updateStatusText(hDlg, "Connecting...");
	updateProgressBar(hDlg, PBM_SETPOS, 25, 0);

	//validate IP address is in correct form
	if (isdigit(dest[0])) {
		if ((retVal = inet_addr(dest)) == -1) {
			appendMessage(hDlg, "Invalid IP Address");
			WSACleanup();
			return;
		}
	}

	//dns query, an IP address would return itself
	if (!(hp = gethostbyname(dest))) {
		updateUI(hDlg, TRUE);
		WSACleanup();
		return;
	}

	updateProgressBar(hDlg, PBM_SETPOS, 50, 0);
	
	//create a socket
	if ((s = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED)) == -1) {
		appendMessage(hDlg, "Cannot create socket, aborting.");
		closesocket(s);
		WSACleanup();
		return;
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(port);
	memcpy((char *)&serverAddr.sin_addr, hp->h_addr, hp->h_length);

	//send request
	if ((retVal = connect(s, (sockaddr *)&serverAddr, sizeof(serverAddr))) == SOCKET_ERROR) {
		retVal = WSAGetLastError();
		//actual error, not just waiting for connection
		if (retVal != WSAEWOULDBLOCK && retVal != WSAEISCONN) {
			appendMessage(hDlg, "\r\nConnect Error");
			updateStatusText(hDlg, "Connection Failed");
			updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
			return;
		}
	}

	updateStatusText(hDlg, "Sending Packets...");
	updateProgressBar(hDlg, PBM_SETPOS, 50, 0);

	buf = static_cast<char*>(malloc(packetSize * sizeof(char)));
	sendBuf.buf = buf;
	sendBuf.len = packetSize;

	if (!file) {
		//fill the packet with d characters
		memset(sendBuf.buf, 'd', getPacketSize(hDlg));
		appendMessage(hDlg, "Sending Packets...");
	}

	time(&timev);

	while(packetNum--) {
		DWORD flags = 0;

		if (file) {
			if (!fin.eof())
				fin.read(sendBuf.buf, sendBuf.len);
			else
				memset(sendBuf.buf, 'd', packetSize);
		}

		//fire away
		retVal = WSASend(s, &sendBuf, 1, &numSent, flags, &ol, compRoutineTCP_c);
		if (retVal == SOCKET_ERROR) {
			retVal = WSAGetLastError();

			if (retVal != WSA_IO_PENDING) {
				updateStatusText(hDlg, "Sending Error, aborted");
				updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
				return;
			}
		}

		setStatNum(hDlg, IDC_PSENT, getStatNum(hDlg, IDC_PSENT) + 1);
		setStatNum(hDlg, IDC_DSENT, getStatNum(hDlg, IDC_DSENT) + packetSize);
	}

	time(&timev2);

	double diff = difftime(timev2, timev);
	std::string t(std::to_string(diff));

	appendMessage(hDlg, "Transfer Time: " + t + " seconds");
	appendMessage(hDlg, "Packets Sent.");

	closesocket(s);
	WSACleanup();
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   clientStartUDP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void clientStartUDP(const char *, const int &, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int port    - The port number specified by the user
--             HWND hDlg   - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      The handle to the dialog is only used to update the user interface.
--             Starts up the client for UDP.
--             Initializes the socket for UDP.
--             Sends all of the datagrams.
----------------------------------------------------------------------------------------------------------------------*/
void clientStartUDP(const char * dest, const int &port, const HWND hDlg) {
	hostent *hp;
	sockaddr_in serverAddr;
	WSADATA wsaData;
	int retVal, packetNum, packetSize, sent;
	DWORD numSent;
	char * buf;
	time_t timev, timev2;
	WSABUF sendBuf = { 0 };
	bool running = TRUE, file = FALSE;
	SOCKET s = NULL;
	WORD wVersionRequested = MAKEWORD(2, 2);
	
	hwnd = hDlg;

	packetNum  = getPacketNum(hDlg);
	packetSize = getPacketSize(hDlg);

	std::ifstream fin(getFileName(hDlg), std::ifstream::in);

	if (fin)
		file = TRUE;

	setStatNum(hDlg, IDC_PSENT, 0);
	setStatNum(hDlg, IDC_DSENT, 0);

	if (WSAStartup(wVersionRequested, &wsaData) != 0) {
		appendMessage(hDlg, "Startup Failed");
		updateStatusText(hDlg, "Startup Failed");
		updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
	}

	//validate IP address is in correct form
	if (isdigit(dest[0])) {
		if ((retVal = inet_addr(dest)) == -1) {
			appendMessage(hDlg, "Invalid IP Address");
			if (s != NULL)
				closesocket(s);
			WSACleanup();
			return;
		}
	}

	//dns query, an IP address would return itself
	if (!(hp = gethostbyname(dest))) {
		updateUI(hDlg, TRUE);
		return;
	}

	updateProgressBar(hDlg, PBM_SETPOS, 50, 0);

	//create a socket
	if ((s = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0, 0, WSA_FLAG_OVERLAPPED)) == -1) {
		appendMessage(hDlg, "Cannot create socket, aborting.");
		closesocket(s);
		WSACleanup();
		return;
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(port);
	memcpy((char *)&serverAddr.sin_addr, hp->h_addr, hp->h_length);

	buf = static_cast<char*>(malloc(packetSize * sizeof(char)));
	sendBuf.buf = buf;
	sendBuf.len = packetSize;

	memset(sendBuf.buf, 'c', sendBuf.len);

	updateStatusText(hDlg, "Sending Datagrams...");
	appendMessage(hDlg, "Sending Datagrams...");

	time(&timev); //get the time

	while (running && packetNum--) {

		OVERLAPPED ol = { 0 };

		if (file) {
			if (!fin.eof())
				fin.read(sendBuf.buf, sendBuf.len);
			else
				memset(sendBuf.buf, 'd', packetSize);
		}

		retVal = WSASendTo(s, &sendBuf, 1, &numSent, 0, (PSOCKADDR)&serverAddr, sizeof(sockaddr_in), &ol, compRoutineUDP_c);
		if (retVal == SOCKET_ERROR) {
			retVal = WSAGetLastError();

			if (retVal != WSA_IO_PENDING) {
				updateStatusText(hDlg, "Sending Error, aborted");
				updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
				running = FALSE;
			}
		}

		updateProgressBar(hDlg, PBM_SETPOS, 50, 0);

		setStatNum(hDlg, IDC_PSENT, getStatNum(hDlg, IDC_PSENT) + 1);
		setStatNum(hDlg, IDC_DSENT, getStatNum(hDlg, IDC_DSENT) + packetSize);
	}

	time(&timev2);

	double diff = difftime(timev2, timev); //subtract to get total time
	std::string t(std::to_string(diff));

	appendMessage(hDlg, "Transfer Time: " + t + " seconds");
	updateStatusText(hDlg, "Datagrams Sent");

	closesocket(s);
	WSACleanup();
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineTCP_c
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineTCP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - Error number (if there is one)
--             DWORD transferred  - Number of bytes transferred in one write to the socket
--             LPWSAOVERLAPPED ol - Overlapped structure operating on the socket
--             DWORD flags        - Optional flags that are invoked by WSASend
--
-- NOTES:      Checks to see if there is an occurance of an error.
--             If there is an error at this point, it can not be ignored and must be addressed.
--             This is only called once the thread is in an alertable, waiting state.
--
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineTCP_c(DWORD error, DWORD transferred, LPWSAOVERLAPPED ol, DWORD flags) {
	if (error) {
		appendMessage(hwnd, "Error in WSASend");
		updateStatusText(hwnd, "Aborting");
	}

	return;
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineUDP_c
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineUDP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - Error number (if there is one)
--             DWORD transferred  - Number of bytes transferred in one write to the socket
--             LPWSAOVERLAPPED ol - Overlapped structure operating on the socket
--             DWORD flags        - Optional flags that are invoked by WSASend
--
-- NOTES:      Checks to see if there is an occurance of an error.
--             If there is an error at this point, it can not be ignored and must be addressed.
--             This is only called once the thread is in an alertable, waiting state.
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineUDP_c(DWORD error, DWORD transferred, LPWSAOVERLAPPED ol, DWORD flags) {
	if (error) {
		appendMessage(hwnd, "Error in WSASendTo");
		updateStatusText(hwnd, "Aborting");
	}

	return;
}