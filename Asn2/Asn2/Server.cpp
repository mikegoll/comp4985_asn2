/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: Server.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   void serverStartTCP(const char *, const int&, const HWND)
--              void serverStartUDP(const char *, const int&, const HWND)
--              void CALLBACK compRoutineTCP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--              void CALLBACK compRoutineUDP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- DATE:        February 6, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Handles all functionality for the server side of the application.
--------------------------------------------------------------------------------------------------*/

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include "Server.h"

//---------- Globals ----------//
commonResources common = { 0 };

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   serverStartTCP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void serverStartTCP(const char *, const int&, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int    port - The port number specified by the user
--             HWND   hDlg - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      Initializes the server for a TCP session.
--             Creates the socket and handles reading for TCP packets.
----------------------------------------------------------------------------------------------------------------------*/
void serverStartTCP(const char * dest, const int& port, const HWND hDlg) {
	int retVal, cLen, pNum, pSize;
	hostent * hp;
	sockaddr_in server;

	char * buf = { 0 };
	SOCKET s = INVALID_SOCKET;

	WORD wVersionRequested = MAKEWORD(2, 2);
	WSADATA wsaData;

	common.hwnd = hDlg;
	common.running = true;
	common.flags = 0;

	memset(&common.wsaol, 0, sizeof(OVERLAPPED));

	if ((retVal = WSAStartup(wVersionRequested, &wsaData)) != 0) {
		return;
	}

	//create the socket for listening
	if ((s = WSASocket(AF_INET, SOCK_STREAM, 0, 0, 0, WSA_FLAG_OVERLAPPED)) == INVALID_SOCKET) {
		showMessageBox(hDlg, "Cannot Create Socket, aborting...", "Socket Creation Error", MB_OK | MB_ICONERROR);
		closesocket(s);
		WSACleanup();
		return;
	}

	memset((char *)&server, 0, sizeof(sockaddr_in));

	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);

	//bind listening socket
	if (retVal = bind(s, (SOCKADDR *)&server, sizeof(SOCKADDR_IN)) == SOCKET_ERROR) {
		showMessageBox(hDlg, "Cannot Bind Socket, Aborting...", "Socket Bind Error", MB_OK | MB_ICONERROR);
		return;
	}

	//listen for up to 5 clients
	if ((retVal = listen(s, 5)) == SOCKET_ERROR) {
		retVal = WSAGetLastError();
		appendMessage(hDlg, "Listen Failed...");
		closesocket(s);
		WSACleanup();
		return;
	}

	pNum  = getPacketNum(hDlg);
	pSize = getPacketSize(hDlg);

	appendMessage(hDlg, "Listening for Clients...");
	updateStatusText(hDlg, "Listening...");
	updateProgressBar(hDlg, PBM_SETPOS, 50, 0);

	//check to see if a file is specified
	if (strcmp(getFileName(hDlg), "") == 0)
		common.file = FALSE;
	else {
		strcpy(common.fileName, getFileName(hDlg));
		common.fout = std::fstream(common.fileName, std::ios_base::out | std::ios::binary);
		//if the file has been opened
		if (common.fout.is_open())
			common.file = TRUE;
		else
			appendMessage(hDlg, "Cannot open file");
	}

	//accept incoming connection
	if ((common.commonSock = accept(s, 0, 0)) == INVALID_SOCKET) {
		//no error, just no connection attempted
		if ((retVal = WSAGetLastError()) != WSAEWOULDBLOCK) {
			//one accept was failed, try again
			appendMessage(hDlg, "Accept Failed");
			common.running = FALSE;
		}
	}

	appendMessage(hDlg, "Client connected");

	//see if the buffer is not initialized
	if (buf == 0) {
		buf = static_cast<char *>(malloc(getPacketSize(hDlg) * sizeof(char)));
		common.wsaBuf.buf = buf;
		common.wsaBuf.len = getPacketSize(hDlg);
	}

	updateStatusText(hDlg, "Receiving Packets...");

	if ((retVal = WSARecv(common.commonSock, &common.wsaBuf, 1, &common.recv, &common.flags,
		&common.wsaol, compRoutineTCP)) == SOCKET_ERROR) {

		if ((retVal = WSAGetLastError()) != WSA_IO_PENDING) {
			appendMessage(hDlg, "WSARecv failed");
			common.running = FALSE;
		}
	} else {
		setStatNum(common.hwnd, IDC_PRECV, getStatNum(common.hwnd, IDC_PRECV) + 1);
	}
	
	//SleepEx will set the thread in an alertable state in which Windows
	//will run the completion routine, it won't otherwise.
	while (common.running) {
		if ((retVal = SleepEx(INFINITE, TRUE)) != WAIT_IO_COMPLETION) {
			appendMessage(hDlg, "Sleep Failed");
			common.running = FALSE;
		}
	} 

	//Cleanup, server is shutting down/stopping
	if (buf != 0)
		free(buf);

	if (common.file)
		common.fout.close();

	closesocket(s);
	cleanup();
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   serverStartUDP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void serverStartUDP(const char *, const int&, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int    port - The port number specified by the user
--             HWND   hDlg - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      Initializes the server for a UDP session.
--             Creates the socket and handles reading for TCP packets.
----------------------------------------------------------------------------------------------------------------------*/
void serverStartUDP(const char * dest, const int& port, const HWND hDlg) {
	int retVal, cLen, pNum, pSize;
	SOCKET s = 0;
	hostent * hp;
	sockaddr_in server;
	DWORD flags, recvd;
	OVERLAPPED ol;
	WSABUF wsaBuf;
	char * buf = { 0 };
	std::ifstream fileout;

	bool running = true;

	WORD wVersionRequested = MAKEWORD(2, 2);
	WSADATA wsaData;

	common.hwnd = hDlg;
	common.running = TRUE;

	//check to see if a file is specified
	if (strcmp(getFileName(hDlg), "") == 0)
		common.file = FALSE;
	else {
		strcpy(common.fileName, getFileName(hDlg));
		common.fout = std::fstream(common.fileName, std::ios_base::out | std::ios::binary);
		//if the file has been opened
		if (common.fout.is_open())
			common.file = TRUE;
		else
			appendMessage(hDlg, "Cannot open file");
	}

	memset(&ol, 0, sizeof(OVERLAPPED));

	if ((retVal = WSAStartup(wVersionRequested, &wsaData)) != 0) {
		return;
	}

	if (s != NULL) {
		closesocket(s);
	}

	//create the socket
	if ((common.commonSock = WSASocket(AF_INET, SOCK_DGRAM, 0, 0, 0, WSA_FLAG_OVERLAPPED)) == -1) {
		showMessageBox(hDlg, "Cannot Create Socket, aborting...", "Socket Creation Error", MB_OK | MB_ICONERROR);
		closesocket(s);
		WSACleanup();
		return;
	}

	memset((char *)&server, 0, sizeof(sockaddr_in));

	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);

	//bind socket
	if ((retVal = bind(common.commonSock, (SOCKADDR *)&server, sizeof(SOCKADDR_IN))) == -1) {
		retVal = WSAGetLastError();
		closesocket(common.commonSock);
		showMessageBox(hDlg, "Cannot Bind Socket, Aborting...", "Socket Bind Error", MB_OK | MB_ICONERROR);
		return;
	}

	if (buf == 0) {
		buf = static_cast<char *>(malloc(getPacketSize(hDlg) * sizeof(char)));
		common.wsaBuf.buf = buf;
		common.wsaBuf.len = getPacketSize(hDlg);
	}

	flags = MSG_PARTIAL;
	
	appendMessage(hDlg, "Listening for datagrams...\r\n");

	updateStatusText(hDlg, "Listening for datagram...");
	updateProgressBar(hDlg, PBM_SETPOS, 50, 0);

	//register comp routine
	if ((retVal = WSARecvFrom(common.commonSock, &common.wsaBuf, 1, &common.recv, &common.flags, 0, 0, &common.wsaol, compRoutineUDP)) == SOCKET_ERROR) {
		retVal = WSAGetLastError();

		if (retVal != WSA_IO_PENDING) {
			appendMessage(hDlg, "Receive Failed");
			closesocket(s);
			return;
		}
	}

	//continuously listen for datagrams
	//SleepEx will set the thread in an alertable state in which Windows
	//will run the completion routine, it won't otherwise.
	while (common.running) {
		if (SleepEx(INFINITE, TRUE) != WAIT_IO_COMPLETION) {
			//error
			break;
		}
	}

	if (buf != 0)
		free(buf);

	if (common.file)
		common.fout.close();

	cleanup();
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineTCP
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineTCP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - The error number according to what error occurred with WSARecv.
--             DWORD transferred  - The number of bytes transferred in the last read.
--             LPWSAOVERLAPPED ol - The overlapped structure for the socket.
--             DWORD flags        - The flags that are set to alter the behaviour of WSARecv.
--
-- NOTES:      Checks to see if an error has occurred within the last WSARecv event.
--             If an error does occur, the operation must be aborted—serious error occurred.
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineTCP(DWORD error, DWORD transferred, LPWSAOVERLAPPED ol, DWORD flags) {
	int retVal;

	if (error) {
		if (error == WSAECONNRESET) {
			appendMessage(common.hwnd, "\r\nClient terminated connection");
			updateStatusText(common.hwnd, "Client Termination");
			updateProgressBar(common.hwnd, PBM_SETPOS, 0, 0);

		}
		else if (error != WSA_OPERATION_ABORTED)
			appendMessage(common.hwnd, "WSARecv Error, aborting.");

		return;
	}

	if (common.running && (transferred > 0)) {

		common.flags = 0;

		setStatNum(common.hwnd, IDC_PRECV, getStatNum(common.hwnd, IDC_PRECV) + 1);
		setStatNum(common.hwnd, IDC_DRECV, getStatNum(common.hwnd, IDC_DRECV) + transferred);
		//appendMessage(common.hwnd, "Read: " + transferred);

		if (common.file) {
			common.fout.write(common.wsaBuf.buf, transferred);
			if (common.fout.bad()) {
				appendMessage(common.hwnd, "Failed to write to file");
				return;
			}
		}

		if ((retVal = WSARecv(common.commonSock, &common.wsaBuf, 1, &common.recv, &common.flags, &common.wsaol, compRoutineTCP)) == SOCKET_ERROR) {
			if ((retVal = WSAGetLastError()) != WSA_IO_PENDING) {
				appendMessage(common.hwnd, "WSARecv failed");
				common.running = FALSE;
			}
		}
	}
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineUDP
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineUDP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - The error number according to what error occurred with WSARecvFrom.
--             DWORD transferred  - The number of bytes transferred in the last read.
--             LPWSAOVERLAPPED ol - The overlapped structure for the socket.
--             DWORD flags        - The flags that are set to alter the behaviour of WSARecvFrom.
--
-- NOTES:      Checks to see if an error has occurred within the last WSARecvFrom event.
--             If an error does occur, the operation must be aborted—serious error occurred.
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineUDP(DWORD error, DWORD transferred, LPWSAOVERLAPPED ol, DWORD flags) {
	int retVal;

	switch (error) {

	//no error here
	case 0:
		updateProgressBar(common.hwnd, PBM_SETPOS, 100, 0);
		setStatNum(common.hwnd, IDC_PRECV, getStatNum(common.hwnd, IDC_PRECV) + 1);
		setStatNum(common.hwnd, IDC_DRECV, getStatNum(common.hwnd, IDC_DRECV) + transferred);
		
		//appendMessage(common.hwnd, "Read: " + std::to_string(transferred));

		updateProgressBar(common.hwnd, PBM_SETPOS, 50, 0);

		//empty the buffer
		memset(common.wsaBuf.buf, 0, sizeof(common.wsaBuf.buf));

		//register again
		if ((retVal = WSARecvFrom(common.commonSock, &common.wsaBuf, 1, &common.recv, &flags, 0, 0, &common.wsaol, compRoutineUDP)) == SOCKET_ERROR) {
			retVal = WSAGetLastError();

			if (retVal != WSA_IO_PENDING) {
				appendMessage(common.hwnd, "Receive Failed");
				closesocket(common.commonSock);
				common.running = FALSE;
				return;
			}
		}

		break;

	//buffer is too small
	case WSAEMSGSIZE:
		appendMessage(common.hwnd, "Buffer too small.");
		common.running = FALSE;
		break;

	case WSA_OPERATION_ABORTED:
		appendMessage(common.hwnd, "Operation Aborted");
		common.running = FALSE;
		break;

	default:
		appendMessage(common.hwnd, "Error in WSARecvFrom");
		common.running = FALSE;
		break;
	}
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   cleanup
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void cleanup()
--
-- RETURNS:    void
--
-- PARAMETER:  none
--
-- NOTES:      Closes the socket and terminates the WinSock DLL usage upon a graceful error handle.
----------------------------------------------------------------------------------------------------------------------*/
void cleanup() {
	closesocket(common.commonSock);
	WSACleanup();
}