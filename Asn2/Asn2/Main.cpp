/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: Main.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
--              int WinMain(GINSTANCE, HINSTANCE, LPSTR, int)
--
-- DATE:        February 1, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Creates and displays the user interface.
--------------------------------------------------------------------------------------------------*/

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#pragma warning (disable: 4996)
#pragma warning (disable: 4096)

#pragma comment(lib, "ComCtl32.lib")
#pragma comment(lib, "ws2_32")

//Win32 Headers
#include <atlstr.h>  
#include <CommCtrl.h>

//Custom Headers
#include "Main.h"

/*-----------------------------------------------------------------------------------------------
-- FUNCTION:   DialogProc
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
--
-- PARAMETER:  HWND hDlg     - A handle to the dialog to spawn the message box.
--             UINT uMsg     - The Window message that is passed to the application
--             WPARAM wParam - The paramater that is passed to the window.
--             LPARAM lParam - Another parameter passed to the window.
--
-- RETURNS:    int
--
-- NOTES:      This function handles the messages that Windows sends to the window.
--
----------------------------------------------------------------------------------------------- */
INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc = (HDC)wParam;
	HANDLE bgThread = NULL;

	switch (uMsg)
	{
	case WM_INITDIALOG:

		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam)) {

		case IDM_HELP:
			MessageBox(hDlg, "1) Select a Mode\n2) Fill in Settings \n3) Click \"Start\"", "Assignment 2 Help", MB_OK | MB_ICONINFORMATION);
			break;

		case IDM_EXIT:
			PostQuitMessage(0);
			break;

		case IDC_CHECK1:
			setCheck(hDlg, IDC_CHECK1, BST_CHECKED);
			setCheck(hDlg, IDC_CHECK2, BST_UNCHECKED);
			EnableWindow(GetDlgItem(hDlg, IDC_PACKETNUM), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_DEST), FALSE);
			Button_Enable(GetDlgItem(hDlg, IDC_OPEN), FALSE);
			Button_Enable(GetDlgItem(hDlg, IDC_SAVE), TRUE);
			break;

		case IDC_CHECK2:
			setCheck(hDlg, IDC_CHECK1, BST_UNCHECKED);
			setCheck(hDlg, IDC_CHECK2, BST_CHECKED);
			EnableWindow(GetDlgItem(hDlg, IDC_PACKETNUM), TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_PACKETSIZE), TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_DEST), TRUE);
			Button_Enable(GetDlgItem(hDlg, IDC_OPEN), TRUE);
			Button_Enable(GetDlgItem(hDlg, IDC_SAVE), FALSE);
			break;

		case IDC_UDP:
			setCheck(hDlg, IDC_UDP, BST_CHECKED);
			setCheck(hDlg, IDC_TCP, BST_UNCHECKED);
			break;

		case IDC_TCP:
			setCheck(hDlg, IDC_TCP, BST_CHECKED);
			setCheck(hDlg, IDC_UDP, BST_UNCHECKED);
			break;

		case IDC_ACTION:
			updateUI(hDlg, FALSE); //disable input fields
			
			//reset stat numbers
			setStatNum(hDlg, IDC_PSENT, 0);
			setStatNum(hDlg, IDC_PRECV, 0);
			setStatNum(hDlg, IDC_DSENT, 0);
			setStatNum(hDlg, IDC_DRECV, 0);

			//reset text indicators
			setText(hDlg, IDC_EDIT1, "");
			setText(hDlg, IDC_EDIT2, "Initializing");

			//This will also start the client or server
			//Runs the logic in the background, UI remains unblocked
			bgThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)getUserInput, (LPVOID)hDlg, 0, 0);

			break;

		case IDC_OPEN:
			getFilePath(hDlg);
			break;

		case IDC_SAVE:
			getFilePath(hDlg);
			break;

		case IDC_STOP:
			if (bgThread != NULL)
				TerminateThread(bgThread, 0);

			WSACleanup();
			updateUI(hDlg, TRUE);
			updateStatusText(hDlg, "Idle");
			updateProgressBar(hDlg, PBM_SETPOS, 0, 0);
			break;

		}
		break;
		break;


	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_CLOSE:
		//terminate running thread if it exists
		if (bgThread != NULL)
			TerminateThread(bgThread, 0);
		
		//cleanup network stuff just in case
		WSACleanup();
		DestroyWindow(hDlg);
	}
	return FALSE;
}

/*------------------------------------------------------------------------------------------------------------
-- FUNCTION:   WinMain
--
-- DATE:       February 1, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  int WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
--
-- PARAMETER:  HINSTANCE hInst         - A handle to the instance, used by the OS
--             HINSTANCE hprevInstance - No longer used by the OS
--             LPSTR lspszCmdParam     - Command line arguments as a Unicode string
--             int nCmdShow            - Flag that states whether the main window will be minimized, ect
--
-- RETURNS:    int
--
-- NOTES:      Main execution entrance point for the application.
--             Creates the main window that will contain the program.
------------------------------------------------------------------------------------------------------------*/

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hprevInstance, LPSTR lspszCmdParam, int nCmdShow)
{
	HWND hDlg;
	MSG msg;
	BOOL ret;
	HMENU hMenu;

	INITCOMMONCONTROLSEX initCtrls;
	initCtrls.dwSize = sizeof(initCtrls);
	initCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&initCtrls);

	hDlg = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_MAIN), 0, DialogProc, 0);
	hMenu = LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU3));
	SetMenu(hDlg, hMenu);
	ShowWindow(hDlg, nCmdShow);
	UpdateWindow(hDlg);


	//set the UI text and stats
	setText(hDlg, IDC_EDIT2, "Idle");
	setStatNum(hDlg, IDC_PSENT, 0);
	setStatNum(hDlg, IDC_PRECV, 0);
	setStatNum(hDlg, IDC_DSENT, 0);
	setStatNum(hDlg, IDC_DRECV, 0);

	while ((ret = GetMessage(&msg, 0, 0, 0)) != 0) {
		if (ret == -1)
			return -1;

		if (!IsDialogMessage(hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}


