#pragma once
/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: Server.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   void serverStartTCP(const char *, const int&, const HWND)
--              void serverStartUDP(const char *, const int&, const HWND)
--              void CALLBACK compRoutineTCP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--              void CALLBACK compRoutineUDP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--              void cleanup()
--
-- DATE:        February 6, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Handles all functionality for the server side of the application.
--------------------------------------------------------------------------------------------------*/

//C++ Headers
#include <sstream>

//Custom Header
#include "Main.h"

//The global struct used by the server side
typedef struct _common {
	SOCKET commonSock;
	WSABUF wsaBuf;
	DWORD recv;
	DWORD flags;
	OVERLAPPED wsaol;
	bool running;
	HWND hwnd;
	char * buf;
	bool file;
	char * fileName;
	std::fstream fout;
} commonResources;

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   serverStartTCP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void serverStartTCP(const char *, const int&, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int    port - The port number specified by the user
--             HWND   hDlg - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      Initializes the server for a TCP session.
--             Creates the socket and handles reading for TCP packets.
----------------------------------------------------------------------------------------------------------------------*/
void serverStartTCP(const char *, const int&, const HWND);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   serverStartUDP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void serverStartUDP(const char *, const int&, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int    port - The port number specified by the user
--             HWND   hDlg - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      Initializes the server for a UDP session.
--             Creates the socket and handles reading for TCP packets.
----------------------------------------------------------------------------------------------------------------------*/
void serverStartUDP(const char *, const int&, const HWND);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineTCP
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineTCP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - The error number according to what error occurred with WSARecv.
--             DWORD transferred  - The number of bytes transferred in the last read.
--             LPWSAOVERLAPPED ol - The overlapped structure for the socket.
--             DWORD flags        - The flags that are set to alter the behaviour of WSARecv.
--
-- NOTES:      Checks to see if an error has occurred within the last WSARecv event.
--             If an error does occur, the operation must be aborted—serious error occurred.
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineTCP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineUDP
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineUDP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - The error number according to what error occurred with WSARecvFrom.
--             DWORD transferred  - The number of bytes transferred in the last read.
--             LPWSAOVERLAPPED ol - The overlapped structure for the socket.
--             DWORD flags        - The flags that are set to alter the behaviour of WSARecvFrom.
--
-- NOTES:      Checks to see if an error has occurred within the last WSARecvFrom event.
--             If an error does occur, the operation must be aborted—serious error occurred.
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineUDP(DWORD, DWORD, LPWSAOVERLAPPED, DWORD);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   cleanup
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void cleanup()
--
-- RETURNS:    void
--
-- PARAMETER:  none
--
-- NOTES:      Closes the socket and terminates the WinSock DLL usage upon a graceful error handle.
----------------------------------------------------------------------------------------------------------------------*/
void cleanup();