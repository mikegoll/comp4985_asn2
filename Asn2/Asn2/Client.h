#pragma once
/*------------------------------------------------------------------------------------------------
-- SOURCE FILE: Main.cpp
--
-- PROGRAM:     COMP4985_Assignment_2
--
-- FUNCTIONS:   void clientStartTCP(const char *, const int &, const HWND)
--              void clientStartUDP(const char *, const int &, const HWND)
--              void CALLBACK compRoutineTCP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--              void CALLBACK compRoutineUDP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- DATE:        February 6, 2017
--
-- DESIGNER:    Michael Goll
--
-- PROGRAMMER:  Michael Goll
--
-- NOTES:       Handles all functionality to the client portion of the application.
--------------------------------------------------------------------------------------------------*/

//Windows Headers
#include <winsock2.h>
#include <Windows.h>

//C/C++ Headers
#include <ctime>
#include <iomanip>
#include <fstream>

//Custom Headers
#include "Main.h"
#include "UI.h"


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   clientStartUDP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void clientStartUDP(const char *, const int &, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int port    - The port number specified by the user
--             HWND hDlg   - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      The handle to the dialog is only used to update the user interface.
--             Starts up the client for UDP.
--             Initializes the socket for UDP.
--             Sends all of the datagrams.
----------------------------------------------------------------------------------------------------------------------*/
void clientStartTCP(const char *, const int &, const HWND);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   clientStartUDP
--
-- DATE:       February 20, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void clientStartUDP(const char *, const int &, const HWND)
--
-- RETURNS:    void
--
-- PARAMETER:  char * dest - The IP address destination given by the user
--             int port    - The port number specified by the user
--             HWND hDlg   - A handle to the dialog to use with the UI updating functions.
--
-- NOTES:      The handle to the dialog is only used to update the user interface.
--             Starts up the client for UDP.
--             Initializes the socket for UDP.
--             Sends all of the datagrams.
----------------------------------------------------------------------------------------------------------------------*/
void clientStartUDP(const char *, const int &, const HWND);


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineTCP_c
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineTCP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - Error number (if there is one)
--             DWORD transferred  - Number of bytes transferred in one write to the socket
--             LPWSAOVERLAPPED ol - Overlapped structure operating on the socket
--             DWORD flags        - Optional flags that are invoked by WSASend
--
-- NOTES:      Checks to see if there is an occurance of an error.
--             If there is an error at this point, it can not be ignored and must be addressed.
--             This is only called once the thread is in an alertable, waiting state.
--
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineTCP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   compRoutineUDP_c
--
-- DATE:       February 21, 2017
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- INTERFACE:  void CALLBACK compRoutineUDP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD)
--
-- RETURNS:    void
--
-- PARAMETER:  DWORD error        - Error number (if there is one)
--             DWORD transferred  - Number of bytes transferred in one write to the socket
--             LPWSAOVERLAPPED ol - Overlapped structure operating on the socket
--             DWORD flags        - Optional flags that are invoked by WSASend
--
-- NOTES:      Checks to see if there is an occurance of an error.
--             If there is an error at this point, it can not be ignored and must be addressed.
--             This is only called once the thread is in an alertable, waiting state.
----------------------------------------------------------------------------------------------------------------------*/
void CALLBACK compRoutineUDP_c(DWORD, DWORD, LPWSAOVERLAPPED, DWORD);